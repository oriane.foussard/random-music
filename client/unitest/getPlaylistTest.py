import unittest

from base.getPlaylist import getPlaylist

class getPlaylistTest(unittest.TestCase):

    def test_artists(self):
        playlist_test = getPlaylist("unitest/test.json", 2)
        artists = ["Rihanna","Nirvana"]
        res = True
        for music in playlist_test['playlist'] :
            res = res & (music["artist"] in artists)
        self.assertTrue(res)

if __name__ == '__main__':
    unittest.main()