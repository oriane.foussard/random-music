import json
import random

from base.getTrack import getTrack

def getPlaylist(fichier, nombre_music):
    with open(fichier, "r") as read_file:
        datas = json.load(read_file)
        weights = []
        for data in datas :
            weights.append(data['note'])
        
        liste_artists = random.choices(datas, weights=weights, k=nombre_music)
        result = []
        for artist in liste_artists :
            result.append((getTrack(artist['artiste']).json()))
    return {"playlist" : result}
