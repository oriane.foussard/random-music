from dotenv import dotenv_values
import requests

def getTrack(artist : str) :
    config = dotenv_values("../.env")
    r= requests.get(config["URL"]+'/random/' + artist)
    return r 
