import unittest
from clientAudioDB.getInformationAlbum import getInformationAlbum
from clientAudioDB.getInformationArtist import getInformationArtist
from clientAudioDB.getInformationTrack import getInformationTrack

class testGetInformationAudioDB(unittest.TestCase):

    def testConnexionAudioDB(self):
        information_artiste = getInformationArtist("nirvana")
        information_album = getInformationAlbum(information_artiste.json()["artists"][0]['idArtist'])
        information_track = getInformationTrack(information_album.json()["album"][0]['idAlbum']) #tous les albums ont au moins une track
        res = (information_artiste.status_code == 200)&(information_album.status_code == 200)&(information_track.status_code == 200)
        self.assertTrue(res)
        return res

if __name__ == '__main__':
    unittest.main()