import unittest
from clientLyricsovh.getInformationLyrics import getInformationLyrics

class testGetInformationLyrics(unittest.TestCase):

    def testConnexionLyrics(self):
        infoLyrics = getInformationLyrics(strArtist= "Rihanna", strTrack= "Diamonds")
        res = (infoLyrics.status_code == 200)
        self.assertTrue(res)
        return res

if __name__ == '__main__':
    unittest.main()