from typing import Optional

from fastapi import FastAPI,status
from fastapi.responses import JSONResponse
from fastapi.responses import HTMLResponse

from base.getRandomTrack import getRandomTrack
from unitest.TestGetInformationLyrics import testGetInformationLyrics
from unitest.TestgetInformationAudioDB import testGetInformationAudioDB

app = FastAPI()


@app.get("/")
def load_data():
    appelsAudiodb = testGetInformationAudioDB().testConnexionAudioDB()
    appelsLyrics = testGetInformationLyrics().testConnexionLyrics()
    if appelsAudiodb & appelsLyrics :
        item = {"disponibilite services" : {"API AudioDB" :appelsAudiodb, "API Lyrics.ovh" :appelsLyrics} }
        return JSONResponse(status_code=status.HTTP_200_OK, content=item)
    else :
        item = {"disponibilite services" : {"API AudioDB" :appelsAudiodb, "API Lyrics.ovh" :appelsLyrics} }
        return JSONResponse(status_code=status.HTTP_424_FAILED_DEPENDENCY , content=item)


@app.get("/random/{artist_name}")
def get_all_words(artist_name):
    return getRandomTrack(artist_name)
