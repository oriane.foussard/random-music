class Album :
    def __init__(self, idAlbum : int, idArtist : str, strAlbum : str) -> None:
        self.idAlbum = idAlbum
        self.idArtist = idArtist
        self.strAlbum = strAlbum
