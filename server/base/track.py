class Track :
    def __init__(self, idTrack : int, idAlbum : str, strTrack : str, strMusicVid : str) -> None:
        self.idTrack = idTrack
        self.idAlbum = idAlbum
        self.strTrack = strTrack
        self.strMusicVid = strMusicVid
