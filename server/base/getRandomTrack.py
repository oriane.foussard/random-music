
import random
from base.album import Album
from base.artist import Artist
from base.track import Track
from clientAudioDB.getInformationArtist import getInformationArtist
from clientAudioDB.getInformationAlbum import getInformationAlbum
from clientAudioDB.getInformationTrack import getInformationTrack
from clientLyricsovh.getInformationLyrics import getInformationLyrics

def getRandomTrack (str_artist) :
    infoArtist = getInformationArtist(str_artist).json()
    if len(infoArtist["artists"]) > 0 :
        artist = Artist(idArtist= infoArtist["artists"][0]['idArtist'], strArtist= infoArtist["artists"][0]['strArtist'] )
        infoAlbums = getInformationAlbum(artist.idArtist).json()
        index_random_album = random.randint(0, (len(infoAlbums["album"])-1))
        album = Album(idAlbum=infoAlbums["album"][index_random_album]['idAlbum'],idArtist=infoAlbums["album"][index_random_album]['idArtist'] , strAlbum= infoAlbums["album"][index_random_album]['strAlbum'])
        infoTracks = getInformationTrack(album.idAlbum).json()
        index_random_track = random.randint(0, (len(infoTracks["track"])-1))
        track = Track(idTrack = infoTracks["track"][index_random_track]['idTrack'], idAlbum = infoTracks["track"][index_random_track]['idAlbum'], strTrack = infoTracks["track"][index_random_track]['strTrack'], strMusicVid = infoTracks["track"][index_random_track]['strMusicVid'])
        infoLyrics = getInformationLyrics(strArtist= artist.strArtist, strTrack= track.strTrack).json()
        if 'lyrics' in infoLyrics.keys() :
            lyrics = infoLyrics['lyrics']
        else :
            lyrics = 'no lyrics'
        return {"artist":artist.strArtist, "title":track.strTrack,"suggested_youtube_url":track.strMusicVid,"lyrics":lyrics}


