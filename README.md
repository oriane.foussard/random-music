# Random Music


## Description

Ce projet contient un serveur de l'API RandomMusic mais également un client de cette API.
L'API RandomMusic permet de récupéré les information d'une music tiré au hazard pour l'artiste que vous désirez. Cette API est basé sur les API : [AudioDB](https://www.theaudiodb.com/api_guide.php) et [Lyrics.ovh](https://lyricsovh.docs.apiary.io/#reference/0/lyrics-of-a-song/search?console=1).
Le client permet de créer des playlist avec les artistes que vous souhaitez.

### Schéma d'architecture
```mermaid
flowchart LR
    A[User] -- json --> B{Client};
    B -- HTTP request --> C{Random Music API};
    C -- HTTP request --> D[AudioDB API];
    C -- HTTP request  --> E[LyricsOvh API];
    B -- json --> A;
    C -- HTTP response --> B;
    D -- HTTP response --> C;
    E -- HTTP response --> C;

```

## Installation du projet

La première étape, installation du projet :

```
git clone https://gitlab.com/oriane.foussard/random-music
cd random-music
pip install -r requirements.txt
```
verfiez que vous posedez une version de `python3`.

## Utilisation de l'API

Pour utiliser l'API random musique, on doit commencer par lancer l'API sur notre machine.
```
cd server
uvicorn mainServer:app --reload
```
### Base URL

L'URL de base pour toute l'API est `http://localhost:8000`. Cette URL est contenu dans le fichier `.env`, il est possible de la modifier si votre API local est sur un autre port.
### État

`GET /`  
Teste la bonne configuration et la disponibilité des services dont dépend l'API.
En cas de succès, l'API renverra :
```json
{
"disponibilite services" : {
    "API AudioDB" : true,
    "API Lyrics.ovh" : true
    }
}
```
Si les appels aboutissent alors le status code sera 200_OK sinon 424_FAILED_DEPENDENCY.
### A random music 

`GET /random/{artist_name}`  
Renvoie, pour un string correspondant au nom d'un artiste donné en entrée, les informations sur une musique de cet artiste au hasard
En cas de succès, l'API renverra :
```json
{
    "artist":"rick astley",
    "title":"Never Gonna Give You Up",
    "suggested_youtube_url":"http://www.youtube.com/watch?v=dQw4w9WgXcQ",
    "lyrics":"We're no strangers to love, You know the rules and so do I"
}
```
## Utilisation du Client 
Pour lancer notre client il suffit de lancer la commande :
```
cd client
python3 mainClient.py
```
Cette commande renverra dans le terminal une liste contenant les information de la playlist créer. Cette playlist est construit à partir des informations contenu dans le fichier `rudy.json`. On peut modifier le nombre de musiques souhaités dans le `mainClient.py` avec l'argument `nombre_music`.

### Test
Il est possible de tester si notre fonction de création de playlist ne contient que les artistes que l'on désire, pour cela il faut lancer le test :
```
cd client
python3 -m unittest unitest/testRamdomMusic.py
```
## Authors

Oriane Foussard.
